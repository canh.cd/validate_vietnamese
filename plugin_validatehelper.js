const Validate_Egov = {
    //intial variable
    isValidate: false,
    mess: "",
    TYPE: {
        CMND: "CMND",
        HOCHIEU: "HOCHIEU",
        CCCD: "CCCD",
        NGAYCAP_CMND: "NGAYCAP_CMND",
        NGAYSINH: "NGAYSINH",
        SODIENTHOAI: "SODIENTHOAI",
        HOTEN: "HOTEN",
        EMAIL: "EMAIL",
    },
    class_Validate: "validatehelper",
    style_input_isValidate: "border:green solid 1px;",
    style_input_notValidate: "border:red solid 1px;",
    class_input_isValidate: "field-validatehelper-success",
    class_input_notValidate: "field-validatehelper-warning",
    attributes_validate: "validate-type",
    setValue(item) {
        //this.isValidate = value
        //default: sau khi valite sẽ hiển thị border 
        try {
            /*
            if (item.isValidate) {
                $("#" + item.id).attr('style', this.style_input_isValidate);
            } else {
                ("#" + item.id).attr('style', this.style_input_notValidate);
            }
            */
            this.ShowUIAfterValidate(item);//hàm custom nếu muốn hiển thị khác
            console.log(item);
            this.mess = "";
        } catch (e) {
            console.log(e)
        }

    },
    extendInput: [],
    listInput: [],
    Initialize() {
        this.listInput = this.extendInput;
        this.extendInput = [];
        var listRawItem = $("." + this.class_Validate);
        if (listRawItem.length > 0) {
            for (let i = 0; i < listRawItem.length; i++) {
                var item = { id: "", value: "", type: "", isValidate: "", mess: "" }
                var tempType = listRawItem[i].attributes[this.attributes_validate].value
                item.type = tempType.split(' ')
                item.id = listRawItem[i].id;
                item.value = listRawItem[i].value;
                this.listInput.push(item);
            }
        }
    },
    TYPE_VALIDATE: {
        'CMND': (item) => { return Validate_Egov.CMNDValidate(item) },
        'HOCHIEU': (item) => { return Validate_Egov.CCCDValidate(item) },
        'NGAYCAP_CMND': (item) => { return Validate_Egov.NgayCapCMNDValidate(item) },
        'NGAYSINH': (item) => { return Validate_Egov.NgaySinhValidate(item) },
        'SODIENTHOAI': (item) => { return Validate_Egov.MobileValidate(item) },
        'HOTEN': (item) => { return Validate_Egov.HotenValidate(item)  },
        'EMAIL': (item) => { return Validate_Egov.EmailValidate(item) },
        'CCCD': (item) => { return Validate_Egov.CCCDValidate(item) },
    },
 
    plugins: {},
 
    validate() {
        this.Initialize();
        //console.log(this.listInput)
        let data = this.listInput;
        for (let i = 0; i < data.length; i++) {
            try {
                for (let n = 0; n < data[i].type.length; n++) {
                    let func = this.TYPE_VALIDATE[data[i].type[n]];
                    var temprs = func(data[i]);
                    this.setValue(temprs);
                    if (temprs.isValidate) {
                        break;
                    }


                }
            } catch (e) {
                console.log(e);
            }
        }
    },
 
    register(plugin) {
        const { name, exec } = plugin;
        this.plugins[name] = exec;
    },
    ShowUIAfterValidate(input) {
        if (input.hasChild) {
            if (input.isValidate) {
                $("#" + input.id).children().addClass(this.class_input_isValidate);
            } else {
                $("#" + input.id).children().addClass(this.class_input_notValidate);
            }
        } else {
            if (input.isValidate) {
                $("#" + input.id).addClass(this.class_input_isValidate);
            } else {
                $("#" + input.id).addClass(this.class_input_notValidate);
            }
        }
    },
    CMNDValidate(item) {
        var pattern = /[0-9]{9}/;
        var status = pattern.test(item.value)
        item.isValidate = status;
        if (!status) {
            item.mess = this.Error.CMND
        }
        return item;
    },
    EmailValidate(item) {
        var pattern = /^(\w+.@\w+.\w{2,4})$/;
        var status = pattern.test(item.value)
        item.isValidate = status;
        if (!status) {
            item.mess = this.Error.Email
        }
        return item;
    },
    MobileValidate(item) {
        var pattern = /^(0\d{9,10})$/;
        var status = pattern.test(item.value)
        item.isValidate = status;
        if (!status) {
            item.mess = this.Error.Mobile
        }
        return item;
    },
    NgaySinhValidate(item) {
        var pattern = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
        var status = pattern.test(item.value)
        var status = pattern.test(item.value)
        item.isValidate = status;
        if (!status) {
            item.mess = this.Error.Date
        }
        return item;


    },
    NgayCapCMNDValidate(item) {
        var pattern = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
        if (pattern.test(item.value)) {
            item.isValidate = true;
            var listdata = Validate_Egov.listInput
            var obj = listdata.find(o => o.type === this.TYPE.NGAYSINH);
            if (obj != null && typeof obj != "undefined") {
                var ngaysinh = moment(obj.value, 'dd/mm/yyyy');
                var ngaycapcmnd = moment(item.value, 'dd/mm/yyyy');
                var yearCheck = ngaycapcmnd.diff(ngaysinh, 'year', true);
                if (yearCheck < 14) {
                    item.mess = this.Error.NotOldEnough;
                    item.isValidate = false
                }

            }
            else {
                item.mess = this.Error.BirthDayNotAvailable;
                item.isValidate = false
            }
        }
        else {
            item.mess = this.Error.Date;
            item.isValidate = false
        }
        return item;

    },
    CCCDValidate(item) {

        if (item.value.length == 12) {
            item.isValidate = true;

        } else {
            item.isValidate = false;
            item.mess = this.Error.INCORECTFORMAT

        }
        return item;

    },
    HotenValidate(item) {
        item.value = item.value.toLowerCase();
        var pattern = /^([a-vxyỳọáầảấờễàạằệếýộậốũứĩõúữịỗìềểẩớặòùồợãụủíỹắẫựỉỏừỷởóéửỵẳẹèẽổẵẻỡơôưăêâđ]+)((\s{1}[a-vxyỳọáầảấờễàạằệếýộậốũứĩõúữịỗìềểẩớặòùồợãụủíỹắẫựỉỏừỷởóéửỵẳẹèẽổẵẻỡơôưăêâđ]+){1,})$/
        var status = pattern.test(item.value)
        item.isValidate = status;
        if (status) {
            var arr = item.value.split(' ');
            for (let i = 0; i < arr.length; i++) {
                var rs = Vietnamese_SpellingCheck.SpellingCheck(arr[i]);
                if (!rs.status) {
                    item.isValidate = false;
                    item.mess = rs.mess;
                    break;
                }
            }
        } else {
            item.mess = this.Error.INCORECTFORMAT;

        }
        return item;
    },
    Error: {
        CMND: "Chứng minh nhân dân sai định dạng!",
        Email: "Email sai định dạng!",
        Mobile: "Số điện thoại sai định dạng!",
        NgaySinh_ChuaDuTuoi: "Chưa đủ tuổi!",
        Date: "Ngày sai định dạng!",
        NotOldEnough: "Chưa đủ tuổi cấp!",
        BirthDayNotAvailable: "Chưa nhập ngày sinh!",
        INCORECTFORMAT: "Sai định dạng!"
    }



};
const Vietnamese_SpellingCheck = {
    TV_AmCuoi: [
        "c", "m", "n", "p", "t",
        "ch", "ng", "nh"
    ],
    TV_AmDau: [
        "b", "c", "h", "d", "đ", "g", "h", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "x",
        "ch", "gh", "kh", "ng", "nh", "ph", "th", "tr",
        "ngh"
    ],
    UrlFileData: "/Library/validate_egov/",
    SpellingCheck(text) {
        var rs = { status: false, mess: "" }
        var _amdau = ""
        var _amcuoi = ""
        var _amgiua = ""
        text = text.trim().toLowerCase();
        // var text = "thuật"
        //kiểm tra âm đầu
        for (let i = 0; i < text.length - 1; i++) {
            var tempText = text.slice(0, i + 1);
            // console.log(tempText)
            var tempAmDau = this.TV_AmDau.find((element) => element === tempText)

            if (typeof tempAmDau !== "undefined") {
                _amdau = tempText;
            }
        }
        //kiểm tra âm cuối
        for (let i = text.length; i > 0; i--) {
            var tempText = text.slice(i - 1, text.length);
            //  console.log(tempText)
            var tempAmCuoi = this.TV_AmCuoi.find((element) => element === tempText)

            if (typeof tempAmCuoi !== "undefined") {
                _amcuoi = tempText;
            }

        }
        var tempAG = text.slice(_amdau.length, text.length - _amcuoi.length);
        // tempAG =removeAccents(tempAG);
        var TV_AmGiua = this.GetDataAmGiua("amgiua");
        var tempAmGiua = TV_AmGiua.find((element) => element === tempAG)
        if (typeof tempAmGiua !==  "undefined") {
            _amgiua = tempAG;
        }
        // 1 từ bắt buộc phải có âm giữa
        if (_amgiua == "") {

            rs.status = false;
            rs.mess = this.Error.error1 + ": " + text;
            //    return false;
        }
        //một số âm giữa bắt buộc phải có âm cuối


        //check âm giữa có thể kết hợp được với âm đầu hay không
        if (_amdau != "") {
            var tempA = this.GetDataByAmDau(_amdau);
            var tempCheck = tempA.find((element) => element == _amgiua)
            if (typeof tempCheck != "undefined") {
                rs.status = true;
                rs.mess = "Âm giữa có thể kết hợp với âm đầu"
                //    console.log("kh  amdau : " + true)
            } else {
                rs.status = false;
                rs.mess = this.Error.error2 + ": " + text;

                //rs.mess = "Âm giữa không thể thể kết hợp với âm đầu"
                //console.log("kh  amdau : " + false)
                //return false;

            }
        } else {//check nếu âm đầu không có thì âm giữa có đứng 1 mình được không
            var tempA = this.GetDataAmGiua("amgiuacothekhongamdau");
            var tempCheck = tempA.find((element) => element === _amgiua)
            if (typeof tempCheck != "undefined") {
                rs.status = true;
                rs.mess = "Âm giữa không cần âm đầu"
                //    console.log("kh non amdau : " + true)
            } else {
                rs.status = false;
                //rs.mess = "Âm giữa cần âm đầu"
                rs.mess = this.Error.error3 + ": " + text;

                //console.log("kh non amdau : " + false)
                //return false;

            }
        }
        //check âm giữa có thể kết hợp được với âm cuối hay không
        if (_amcuoi != "") {
            var tempAA = this.GetDataByAmCuoi(_amcuoi);
            var tempCheckA = tempAA.find((element) => element == _amgiua)
            if (typeof tempCheckA != "undefined") {
                rs.status = true;
                rs.mess = "Âm giữa không thể kết hợp với âm cuối"

                /*         console.log("kh  amcuoi : " + true)*/
            } else {
                rs.status = false;
                //rs.mess = "Âm giữa không thể kết hợp với âm cuối"
                rs.mess = this.Error.error4 + ": " + text;

                //console.log("kh  amcuoi : " + false)
                //return false;

            }
        }
        else {//check nếu âm cuối không có thì âm giữa có đứng 1 mình được không
            var tempAA = this.GetDataAmGiua("amgiuacothekoamcuoi");
            var tempCheckA = tempAA.find((element) => element == _amgiua)
            if (typeof tempCheckA != "undefined") {
                //console.log("kh non amcuoi : " + true)
                rs.status = true;
                rs.mess = "Âm giữa không cần âm cuối"

            } else {
                rs.status = false;
                rs.mess = this.Error.error7 + ": " + text;

            //    rs.mess = "Âm giữa  cần âm cuối"
            }
        }

        //check âm giữa không cần kết hợp âm đầu và âm cuối

        if (_amcuoi == "" && _amdau == "") {
            var tempA = this.GetDataAmGiua("selfamgiua");
            var tempCheck = tempA.find((element) => element === _amgiua)
            if (typeof tempCheck != "undefined") {
                rs.status = true;
                //rs.mess = "Âm giữa  có thể đứng một mình" + ": " + text;
                //    console.log("check am giữa đứng 1 mình : " + true)
            } else {
                rs.status = false;
                //rs.mess = "Âm giữa này không thể đứng một mình"
                rs.mess = this.Error.error5 + ": " + text;

                //return false;

            }
        }
        // kiểm tra lỗi chính tả về dấu
        var tempD = this.GetDataLoiDau();
        var tempCheckD = tempD.find((element) => element === text)
        if (typeof tempCheckD != "undefined") {
            rs.status = false;
            //rs.mess = "Sai dấu hỏi ngã: " + text
            rs.mess = this.Error.error6 + ": " + text;

            //console.log("kiểm tra dấu hỏi ngã : " + false)
            //return false;

        } else {
            rs.status = true;
            rs.mess = ""
            //console.log("kiểm tra dấu hỏi ngã  : " + true)

        }

        //console.log("Âm đầu: ", _amdau)
        //console.log("Âm giữa: ", _amgiua)
        //console.log("Âm cuối: ", _amcuoi)
        return rs;
    },

    Error: {
        error1: "Không có âm giữa!",
        error2: "Âm giữa không thể thể kết hợp với âm đầu!",
        error3: "Âm giữa cần âm đầu!",
        error4: "Âm giữa không thể kết hợp với âm cuối!",
        error5: "Âm giữa này không thể đứng một mình!",
        error6: "Sai dấu hỏi ngã!",
        error7: "Âm giữa  cần âm cuối!",

    },
    GetDataByAmDau(file) {
        var rs = [];
        $.ajax({
            type: "GET",
            async: false,

            url: this.UrlFileData + "amdau/" + file + ".txt",
            success: function (text) {
                var lines = text.replaceAll('\r', '').split('\n');
                var tempAmGiua = lines.find((element) => element.replace('\r', '') === "ố")
                rs = lines;
            },
            error: function () {
               console.log("data not available");
            }
        });
        return rs;
    },
    GetDataByAmCuoi(file) {
        var rs = [];
        $.ajax({
            type: "GET",
            async: false,

            url: this.UrlFileData + "amcuoi/" + file + ".txt",
            success: function (text) {
                var lines = text.replaceAll('\r', '').split('\n');
                var tempAmGiua = lines.find((element) => element.replace('\r', '') === "ố")
                rs = lines;
                //if (typeof tempAmGiua != "undefined") {
                //    return true;
                //}
                //else {
                //    return false;
                //}
            },
            error: function () {
                return false;
            }
        });
        return rs;
    },
    GetDataAmGiua(file) {
        var rs = [];
        $.ajax({
            type: "GET",
            async: false,
            url: Vietnamese_SpellingCheck.UrlFileData + "amgiua/" + file + ".txt",
            success: function (text) {
                var lines = text.replaceAll('\r', '').split('\n');
                rs = lines;
                //if (typeof tempAmGiua != "undefined") {
                //    return true;
                //}
                //else {
                //    return false;
                //}
            },
            error: function () {
                return false;
            }
        });
        return rs;
    },
    GetDataLoiDau() {
        var rs = [];
        $.ajax({
            type: "GET",
            async: false,
            url: this.UrlFileData + "checkhoinga.txt",
            success: function (text) {
                var lines = text.replaceAll('\r', '').split('\n');
                rs = lines;
            },
            error: function () {
                return false;
            }
        });
        return rs;
    },
};
const VietnameseName_Autocomplete = {
    UrlFileData: "/Library/validate_egov/",
    ListTenThongDung: [],
    GetDataTenThongDung() {
        var rs = [];
        $.ajax({
            type: "GET",
            async: false,
            url: VietnameseName_Autocomplete.UrlFileData + "ten.txt",
            success: function (text) {
                //text = JSON.stringify(text);
                //console.log(text);

                var lines = text.replaceAll('\r', '').split('\n');
                rs = lines;
            },
            error: function () {
              return false;
            }
        });
        this.ListTenThongDung = rs;
    },
    Autocomplete(inp) {
        var arr = this.ListTenThongDung;
        /*the autocomplete function takes two arguments,
        the text field element and an array of possible autocompleted values:*/
        var currentFocus;
        /*execute a function when someone writes in the text field:*/
        inp.addEventListener("input", function (e) {
            var a, b, i, val = this.value;
            /*close any already open lists of autocompleted values*/
            closeAllLists();
            if (!val) { return false; }

            var temp = val.split(" ");
            if (temp.length <= 1) {
                return false;
            } else {
                a, b, i, val = temp[temp.length - 1]
            }
            currentFocus = -1;
            /*create a DIV element that will contain the items (values):*/
            a = document.createElement("DIV");
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            /*append the DIV element as a child of the autocomplete container:*/
            this.parentNode.appendChild(a);
            /*for each item in the array...*/
            for (i = 0; i < arr.length; i++) {
                /*check if the item starts with the same letters as the text field value:*/
                if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                    /*create a DIV element for each matching element:*/
                    b = document.createElement("DIV");
                    /*make the matching letters bold:*/
                    b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                    b.innerHTML += arr[i].substr(val.length);
                    /*insert a input field that will hold the current array item's value:*/
                    b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                    /*execute a function when someone clicks on the item value (DIV element):*/
                    b.addEventListener("click", function (e) {
                        /*insert the value for the autocomplete text field:*/
                        inp.value = "";
                        for (let i = 0; i < temp.length - 1; i++) {
                            inp.value += temp[i] + " ";
                        }

                        inp.value += this.getElementsByTagName("input")[0].value;
                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();
                    });
                    a.appendChild(b);
                }
            }
        });
        /*execute a function presses a key on the keyboard:*/
        inp.addEventListener("keydown", function (e) {
            var x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");
            if (e.keyCode == 40) {
                /*If the arrow DOWN key is pressed,
                increase the currentFocus variable:*/
                currentFocus++;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 38) { //up
                /*If the arrow UP key is pressed,
                decrease the currentFocus variable:*/
                currentFocus--;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 13) {
                /*If the ENTER key is pressed, prevent the form from being submitted,*/
                e.preventDefault();
                if (currentFocus > -1) {
                    /*and simulate a click on the "active" item:*/
                    if (x) x[currentFocus].click();
                }
            }
        });
        function addActive(x) {

            /*a function to classify an item as "active":*/
            if (!x) return false;
            /*start by removing the "active" class on all items:*/
            removeActive(x);
            if (currentFocus >= x.length) currentFocus = 0;
            if (currentFocus < 0) currentFocus = (x.length - 1);
            /*add class "autocomplete-active":*/
            x[currentFocus].classList.add("autocomplete-active");
        }
        function removeActive(x) {
            /*a function to remove the "active" class from all autocomplete items:*/
            for (var i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
            }
        }
        function closeAllLists(elmnt) {
            /*close all autocomplete lists in the document,
            except the one passed as an argument:*/
            var x = document.getElementsByClassName("autocomplete-items");
            for (var i = 0; i < x.length; i++) {
                if (elmnt != x[i] && elmnt != inp) {
                    x[i].parentNode.removeChild(x[i]);
                }
            }
        }
        /*execute a function when someone clicks in the document:*/
        document.addEventListener("click", function (e) {
            closeAllLists(e.target);
        });
    }
}

//function 

//  Plugin
//const CMNDValidate = {
//    name: 'CMND',
//    exec: function (text) {  
//        var pattern = /[0-9]{9}/;
//        return pattern.test(text)
//    }
//}
