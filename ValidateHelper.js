/// inclue <script src="https://momentjs.com/downloads/moment.js"></script>
const TV_AmDau = [
    "b", "c", "h", "d", "đ", "g", "h", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "x",
    "ch", "gh", "kh", "ng", "nh", "ph", "th", "tr",
    "ngh"
]
const TV_AmCuoi = [
    "c", "m", "n", "p", "t",
    "ch", "ng", "nh"
]
const TYPE_VALIDATE = {
    CMND: "CMND",
    HOCHIEU: "HOCHIEU",
    CCCD: "CCCD",
    NGAYCAP_CMND: "NGAYCAP_CMND",
    NGAYSINH: "NGAYSINH",
    SODIENTHOAI: "SODIENTHOAI",
    HOTEN: "HOTEN",
    EMAIL: "EMAIL",
}

function Initialize() {
    listInput = [];
    var listRawItem = $(".validatehelper");
    if (listRawItem.length > 0) {
        for (let i = 0; i < listRawItem.length; i++) {
            var item = { id: "", value: "", type: "", isValidate: "", mess: "" }

            item.type = listRawItem[i].attributes['validate-type'].value
            item.id = listRawItem[i].id;
            item.value = listRawItem[i].value;
            listInput.push(item);
        }
    }
}
function Validate() {
    Initialize()
    for (let i = 0; i < listInput.length; i++) {
        var isValidate = false;
        if (listInput[i].type == TYPE_VALIDATE.CMND) {
            isValidate = CMNDValidate(listInput[i])

        } else if (listInput[i].type == TYPE_VALIDATE.EMAIL) {
            isValidate = EmailValidate(listInput[i])

        }
        else if (listInput[i].type == TYPE_VALIDATE.SODIENTHOAI) {
            isValidate = MobileValidate(listInput[i])

        }
        else if (listInput[i].type == TYPE_VALIDATE.NGAYSINH) {
            isValidate = NgaySinhValidate(listInput[i])

        }
        else if (listInput[i].type == TYPE_VALIDATE.NGAYCAP_CMND) {
            isValidate = NgayCapCMNDValidate(listInput[i])

        }
        else if (listInput[i].type == TYPE_VALIDATE.HOTEN) {
            isValidate = HotenValidate(listInput[i])

        } else if (listInput[i].type == TYPE_VALIDATE.CCCD) {
            isValidate = CCCDValidate(listInput[i])

        }

        if (isValidate) {
            listInput[i].isValidate = true;
            //$("#" + listInput[i].id).attr('style', 'border:green solid 1px;');
            $("#" + listInput[i].id).addClass('field-validatehelper-success');
        } else {
            listInput[i].isValidate = false;
            listInput[i].mess = "";
            //$("#" + listInput[i].id).attr('style', 'border:yellow solid 1px;');
            $("#" + listInput[i].id).addClass('field-validatehelper-warning');
        }
    }
}
var mess = "";
function CMNDValidate(item) {
    var pattern = /[0-9]{9}/;
    return pattern.test(item.value);

}
function EmailValidate(item) {
    var pattern = /^(\w+.@\w+.\w{2,4})$/;
    return pattern.test(item.value);
}
function MobileValidate(item) {
    var pattern = /^(0\d{9,10})$/;
    return pattern.test(item.value);
}
function NgaySinhValidate(item) {
    var pattern = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
    if (pattern.test(item.value)) {
        var obj = listInput.find(o => o.type === TYPE.NGAYCAP_CMND);
        if (obj != null) {
            var ngaysinh = moment(item.value, 'dd/mm/yyyy');
            var ngaycapcmnd = moment(obj.value, 'dd/mm/yyyy');
            var yearCheck = ngaycapcmnd.diff(ngaysinh, 'year');
            if (yearCheck >= 14) {
                return true;
            }
            else {
                mess = "Chưa đủ tuổi cấp CMND";
                return false;
            }
        } else {
            mess = "chưa nhập ngày cấp cmnd"
            return false;
        }
    } else {
        mess = "Sai định dạng!"
        return false
    }


}
function NgayCapCMNDValidate(item) {
    var pattern = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
    if (pattern.test(item.value)) {
        var obj = listInput.find(o => o.type === TYPE.NGAYSINH);
        if (obj != null) {
            var ngaysinh = moment(obj.value, 'dd/mm/yyyy');
            var ngaycapcmnd = moment(item.value, 'dd/mm/yyyy');
            var yearCheck = ngaycapcmnd.diff(ngaysinh, 'year');
            if (yearCheck >= 14) {
                return true;
            }
            else {
                mess = "Chưa đủ tuổi cấp CMND";
                return false;
            }
        } else {
            mess = "chưa nhập ngày cấp cmnd"
            return false;
        }
    } else {
        mess = "Sai định dạng!"
        return false
    }

}

function HotenValidate(item) {

    item.value = item.value.toLowerCase();
    var pattern = /^([a-vxyỳọáầảấờễàạằệếýộậốũứĩõúữịỗìềểẩớặòùồợãụủíỹắẫựỉỏừỷởóéửỵẳẹèẽổẵẻỡơôưăêâđ]+)((\s{1}[a-vxyỳọáầảấờễàạằệếýộậốũứĩõúữịỗìềểẩớặòùồợãụủíỹắẫựỉỏừỷởóéửỵẳẹèẽổẵẻỡơôưăêâđ]+){1,})$/
    /*    if (pattern.test(item.value)) {*/
    if (true) {
        var arr = item.value.split(' ');
        //console.log(arr)
        for (let i = 0; i < arr.length; i++) {
            if (!SpellingCheck(arr[i])) {
                return false;
            }
        }
        return true;
    }
}

function CCCDValidate(item) {

    if (item.value.length == 12) {
        return true;

    } else {
        return false;
    }

}



function SpellingCheck(text) {
    var _amdau = "";
    var _amcuoi = "";
    var _amgiua = "";
    text = text.trim().toLowerCase();
    // var text = "thuật"
    //kiểm tra âm đầu
    for (let i = 0; i < text.length - 1; i++) {
        var tempText = text.slice(0, i + 1);
        // console.log(tempText)
        var tempAmDau = TV_AmDau.find((element) => element === tempText)

        if (typeof tempAmDau != "undefined") {
            _amdau = tempText;
        }
    }
    //kiểm tra âm cuối
    for (let i = text.length; i > 0; i--) {
        var tempText = text.slice(i - 1, text.length);
        //  console.log(tempText)
        var tempAmCuoi = TV_AmCuoi.find((element) => element === tempText)

        if (typeof tempAmCuoi != "undefined") {
            _amcuoi = tempText;
        }

    }
    var tempAG = text.slice(_amdau.length, text.length - _amcuoi.length);
    // tempAG =removeAccents(tempAG);
    var TV_AmGiua = GetDataAmGiua("amgiua");
    var tempAmGiua = TV_AmGiua.find((element) => element === tempAG)
    if (typeof tempAmGiua != "undefined") {
        _amgiua = tempAG;
    }
    // 1 từ bắt buộc phải có âm giữa
    if (_amgiua == "") {
        
        return false;
    }
    //một số âm giữa bắt buộc phải có âm cuối


    //check âm giữa có thể kết hợp được với âm đầu hay không
    if (_amdau != "") {
        var tempA = GetDataByAmDau(_amdau);
        var tempCheck = tempA.find((element) => element == _amgiua)
        if (typeof tempCheck != "undefined") {
            console.log("kh  amdau : " + true)
        } else {
            console.log("kh  amdau : " + false)
            return false;

        }
    } else {//check nếu âm đầu không có thì âm giữa có đứng 1 mình được không
        var tempA = GetDataAmGiua("amgiuacothekhongamdau");
        var tempCheck = tempA.find((element) => element === _amgiua)
        if (typeof tempCheck != "undefined") {
            console.log("kh non amdau : " + true)
        } else {
            console.log("kh non amdau : " + false)
            return false;

        }
    }
    //check âm giữa có thể kết hợp được với âm cuối hay không
    if (_amcuoi != "") {
        var tempAA = GetDataByAmCuoi(_amcuoi);
        var tempCheckA = tempAA.find((element) => element == _amgiua)
        if (typeof tempCheckA != "undefined") {
            console.log("kh  amcuoi : " + true)
        } else {
            console.log("kh  amcuoi : " + false)
            return false;

        }
    } else {//check nếu âm cuối không có thì âm giữa có đứng 1 mình được không
        var tempAA = GetDataAmGiua("amgiuacothekoamcuoi");
        var tempCheckA = tempAA.find((element) => element == _amgiua)
        if (typeof tempCheckA != "undefined") {
            console.log("kh non amcuoi : " + true)
            
        } else {
            console.log("kh non amcuoi : " + false)
            return false
        }
    }

    //check âm giữa không cần kết hợp âm đầu và âm cuối

    if (_amcuoi == "" && _amdau == "") {
        var tempA = GetDataAmGiua("selfamgiua");
        var tempCheck = tempA.find((element) => element === _amgiua)
        if (typeof tempCheck != "undefined") {
            console.log("check am giữa đứng 1 mình : " + true)
        } else {
            console.log("check am giữa đứng 1 mình  : " + false)
            return false;

        }
    }
    // kiểm tra lỗi chính tả về dấu
    var tempD = GetDataLoiDau();
    var tempCheckD = tempD.find((element) => element === text)
    if (typeof tempCheckD != "undefined") {
        console.log("kiểm tra dấu hỏi ngã : " + false)
        return false;

    } else {
        console.log("kiểm tra dấu hỏi ngã  : " + true)

    }

    //var tempE = GetDataTenThongDung();
    //var tempCheckE = tempE.find((element) => similarity(element.toLowerCase(), text.toLowerCase())  ==1)
    //if (typeof tempCheckE != "undefined") {
    //    console.log("kiểm tra tên thông dụng: " + false)

    //} else {
    //    console.log("kiểm tra tên thông dụng : " + true)
    //    return false;

    //}
    console.log("Âm đầu: ", _amdau)
    console.log("Âm giữa: ", _amgiua)
    console.log("Âm cuối: ", _amcuoi)
    return true;
}



//get data
var urlFileValidate = "/Library/validate_egov/";
function GetDataByAmDau(file) {
    var rs = [];
    $.ajax({
        type: "GET",
        async: false,

        url: urlFileValidate+ "amdau/" + file + ".txt",
        success: function (text) {
            var lines = text.replaceAll('\r', '').split('\n');
            var tempAmGiua = lines.find((element) => element.replace('\r', '') === "ố")
            rs = lines;
            //if (typeof tempAmGiua != "undefined") {
            //    return true;
            //}
            //else {
            //    return false;
            //}
        },
        error: function () {
            return false;
        }
    });
    return rs;
}
function GetDataByAmCuoi(file) {
    var rs = [];
    $.ajax({
        type: "GET",
        async: false,

        url: urlFileValidate + "amcuoi/" + file + ".txt",
        success: function (text) {
            var lines = text.replaceAll('\r', '').split('\n');
            var tempAmGiua = lines.find((element) => element.replace('\r', '') === "ố")
            rs = lines;
            //if (typeof tempAmGiua != "undefined") {
            //    return true;
            //}
            //else {
            //    return false;
            //}
        },
        error: function () {
            return false;
        }
    });
    return rs;
}



function GetDataAmGiua(file) {
    var rs = [];
    $.ajax({
        type: "GET",
        async: false,
        url: urlFileValidate + "amgiua/" + file + ".txt",
        success: function (text) {
            var lines = text.replaceAll('\r', '').split('\n');
            rs = lines;
            //if (typeof tempAmGiua != "undefined") {
            //    return true;
            //}
            //else {
            //    return false;
            //}
        },
        error: function () {
            return false;
        }
    });
    return rs;
}
function GetDataLoiDau() {
    var rs = [];
    $.ajax({
        type: "GET",
        async: false,
        url: urlFileValidate +"checkhoinga.txt",
        success: function (text) {
            var lines = text.replaceAll('\r', '').split('\n');
            rs = lines;
        },
        error: function () {
            return false;
        }
    });
    return rs;
}
function GetDataTenThongDung() {
    var rs = [];
    $.ajax({
        type: "GET",
        async: false,
        url: urlFileValidate + "ten.txt",
        success: function (text) {
            var lines = text.replaceAll('\r', '').split('\n');
            rs = lines;
        },
        error: function () {
            return false;
        }
    });
    return rs;
}

//
//gợi ý tên
var tenthongdung = [];
$(document).ready(function () {
    tenthongdung = GetDataTenThongDung();
});

function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false; }

        var temp = val.split(" ");
        if (temp.length <= 1) {
            return false;
        } else {
            a, b, i, val = temp[temp.length - 1]
        }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function (e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = "";
                    for (let i = 0; i < temp.length - 1; i++) {
                        inp.value += temp[i] + " ";
                    }

                    inp.value += this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {

        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}


